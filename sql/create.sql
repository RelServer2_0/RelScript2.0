#drop database IF EXISTS porgv2;
#create database porgv2 character set utf8mb4 COLLATE = utf8mb4_unicode_ci;


#use porgv2;  
#set global max_allowed_packet = 100*1024*1024;
#max_allowed_packet = 100 M    #2017/11/14添加  wbw  添加 最大值

set SQL_SAFE_UPDATES =0;
#ceiling(rand() * 100)
#update t_qa set viewsadd =  FLOOR(800 + (RAND() * 200)) where viewsadd > 1000

#创建版本信息
insert into t_com_configure(app,version,sysmode,feechannel,feeplatform,feeuser,iswxpay,isalipay,iswithdraw,isrecharge) values (11,"v1.0.7",-1,0,0,1,-1,-1,-1,-1);
insert into t_com_configure(app,version,sysmode,feechannel,feeplatform,feeuser,iswxpay,isalipay,iswithdraw,isrecharge) values (12,"v1.0.7",-1,0,0,1,-1,-1,-1,-1);


#########################权限#########################
insert into t_acs(module,title,brief,acscode) values
(1,'资料编辑','团队基本信息编辑，系统配置',101),
(1,'成员管理','成员添加和移除',102),
(1,'用户管理','冻结，禁上传/下载/提现',103),
(1,'主页查看','查看团队主页及成员',104),

(5,'交易维护','交易账户数据维护',501),
(5,'购买拍点','为团队购买拍点',502),
(5,'下载图片','使用团队拍点下载图片',503),
(5,'审核图片','已发布图片下架，图片分类',504),
(5,'美化数据','图片、用户数据美化',505),
(5,'标签维护','审核，分类，去重',506),
(5,'活动公告','运营主题图活动、公告/通知',507);



#########################实名认证支付商品orgid对应app#########################
insert into t_goods(orgid,types,title,icon) values (11,5,'实名认证','http://op4axvn75.bkt.clouddn.com/LOGO.png');
insert into t_goods_pkg(goodsid,subject,body,price,isdef) values (1,'微信支付1分钱','通过微信支付判断用户是否实名',1,1);

insert into t_goods(orgid,types,title,icon) values (12,5,'实名认证','http://op4axvn75.bkt.clouddn.com/LOGO.png');
insert into t_goods_pkg(goodsid,subject,body,price,isdef) values (2,'微信支付1分钱','通过微信支付判断用户是否实名',1,1);

#插入充值虚拟币
insert into t_goods(orgid,types,title,icon) values (11,6,'拍点套餐','http://op4axvn75.bkt.clouddn.com/LOGO.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (3,'19900拍点（节省￥9.00）','',190001900,19900,-1);
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (3,'29900拍点（节省￥39.00）','',26000,29900,-1);
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (3,'39900拍点（节省￥69.00）','',33000,39900,1);

insert into t_goods(orgid,types,title,icon) values (12,6,'拍点套餐','http://op4axvn75.bkt.clouddn.com/LOGO.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (4,'19900拍点（节省￥9.00）','',19000,19900,-1);
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (4,'29900拍点（节省￥39.00）','',26000,29900,-1);
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (4,'39900拍点（节省￥69.00）','',33000,39900,1);


#插入红包奖励金
insert into t_goods(orgid,types,title,icon) values (11,4,'上传图片红包奖励','http://op4axvn75.bkt.clouddn.com/redcarsh.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (5,'上传图片1元以下随机奖励','',100,100,1);

insert into t_goods(orgid,types,title,icon) values (11,4,'下载图片红包奖励','http://op4axvn75.bkt.clouddn.com/redcarsh.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (6,'下载图片1元以下随机奖励','',100,100,1);

insert into t_goods(orgid,types,title,icon) values (12,4,'上传图片红包奖励','http://op4axvn75.bkt.clouddn.com/redcarsh.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (7,'上传图片1元以下随机奖励','',100,100,1);

insert into t_goods(orgid,types,title,icon) values (12,4,'下载图片红包奖励','http://op4axvn75.bkt.clouddn.com/redcarsh.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (8,'下载图片1元以下随机奖励','',100,100,1);

#1分钱红包测试
insert into t_goods(orgid,types,title,icon) values (12,4,'红包测试','http://op4axvn75.bkt.clouddn.com/redcarsh.png');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (1380,'给我发1分钱普通红包测试','',1,1,1);

#插入测试商品
insert into t_goods(orgid,types,title,des) values (11,-1,'入场/手袋/卡牌/身份劵','微信支付，1元钱测试，连续一个月开通提现');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (9,'参加活动入场领取手袋/卡牌/身份劵','',100,100,1);
insert into t_goods(orgid,types,title,des) values (12,-1,'入场/手袋/卡牌/身份劵','微信支付，1元钱测试，连续一个月开通提现');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (10,'参加活动入场领取手袋/卡牌/身份劵','',100,100,1);


#插入服务工具
insert into t_goods(orgid,types,title,icon,des) values (1,7,'发布活动','https://ccian.com/%E5%8F%91%E5%B8%83%E6%B4%BB%E5%8A%A8@3x.png','活动发布预告、在线报名、信息审核、朋友圈分享');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通活动发布服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'组织架构','https://ccian.com/%E7%BB%84%E7%BB%87%E6%9E%B6%E6%9E%84@3x.png','组织架构分组、子母账号、管理运营权限分配');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通织管理服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'任务策划','https://ccian.com/%E4%BB%BB%E5%8A%A1%E7%AD%96%E5%88%92@3x.png','给指定成员分配任务，成员收到领取时汇报截止时间，看板形式展示');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通织任务策划服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'会议','https://ccian.com/%E5%B9%BF%E5%91%8A%E8%81%94%E7%9B%9F@3x.png','会议发起记录');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通会议服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'上架票务','https://ccian.com/%E4%B8%8A%E6%9E%B6%E7%A5%A8%E5%8A%A1@3x.png','上架入场票务，灵活变现');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通票务支付服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'会员卡','https://ccian.com/%E4%BC%9A%E5%91%98%E5%8D%A1@3x.png','用户分级，发放会员刺激用户升级');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通会员服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'数据统计','https://ccian.com/%E6%95%B0%E6%8D%AE%E7%BB%9F%E8%AE%A1@3x.png','活动报名、签到，票务销售等数据统计');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通数据统计服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'相册','https://ccian.com/%E7%9B%B8%E5%86%8C@3x.png','团队相册，活动相册图片直播');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通相册服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'文章评论','https://ccian.com/%E6%96%87%E7%AB%A0%E8%AF%84%E8%AE%BA@3x.png','发布公告文章');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通文章评论管理服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'资料库','https://ccian.com/%E7%9B%B8%E5%86%8C@3x.png','云端存储资料，自动归档所有图片、PDF、文档');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通资料库文件夹服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'抽奖','https://ccian.com/%E6%8A%BD%E5%A5%96@3x.png','使用抽奖小工具发起抽奖，轻松营销');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通抽奖服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'签到墙','https://ccian.com/%E5%B9%BF%E5%91%8A%E8%81%94%E7%9B%9F@3x.png','活动现场，签到互动');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通签到墙服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'海报工场','https://ccian.com/%E6%B5%B7%E6%8A%A5%E5%B7%A5%E5%9C%BA@3x.png','活动海报一键生成，精致模板，快速分享');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通海报工场服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'问卷投票','https://ccian.com/%E6%8A%95%E7%A5%A8@3x.png','投票快手是发起投票、问卷的效率工具。致力于提供：精致的活动投票、市场问卷模板；极简的输入体验，灵活的逻辑设置；快捷的分享和实时数据服务；实乃活动运营必备小公举
');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通问卷投票服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'周报','https://ccian.com/%E6%96%87%E7%AB%A0%E8%AF%84%E8%AE%BA@3x.png','成员周报服务，评估成员工作及任务');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通周报服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'迷你简历','https://ccian.com/%E8%BF%B7%E4%BD%A0%E7%AE%80%E5%8E%86@3x.png','简历制作效率工具，致力于提供精致的简历模板，极致的输入体验，快捷的分享和投递服务');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通迷你简历服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'服务商','https://ccian.com/%E6%9C%8D%E5%8A%A1%E5%95%86@3x.png','认证活动服务商，出售策划方案，活动道具，礼仪等服务');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通服务商服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'拉赞助','https://ccian.com/%E6%8B%89%E8%B5%9E%E5%8A%A9@3x.png','赞助商广场，你办活动我买单，轻松盈利');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通拉赞助服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'场地管理','https://ccian.com/%E5%9C%BA%E5%9C%B0%E7%AE%A1%E7%90%86@3x.png','活动场地地址管理，智能LBS服务');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通场地管理服务','',100,100,1);

insert into t_goods(orgid,types,title,icon,des) values (1,7,'广告联盟','https://ccian.com/%E5%B9%BF%E5%91%8A%E8%81%94%E7%9B%9F@3x.png','流量联盟，活动主办方、服务商广告互投粉丝共享');
insert into t_goods_pkg(goodsid,subject,body,price,pricecoin,isdef) values (0,'开通广告联盟服务','',100,100,1);


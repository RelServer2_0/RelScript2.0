
#########################标签#########################
#//第1类
#//风景：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'风景','http://op4axvn75.bkt.clouddn.com/%E9%A3%8E%E6%99%AF.png','fengjing');

#//第2类
#//花草：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'花草','http://op4axvn75.bkt.clouddn.com/%E8%8A%B1%E8%8D%89.png','花园 huayuan');

#//第3类
#//人：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'人','http://op4axvn75.bkt.clouddn.com/%E4%BA%BA.png','人物 renwu');

#//第4类
#//开心：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'开心','http://op4axvn75.bkt.clouddn.com/LOGO.png','快乐 kuaile');

#//第5类
#//宠物：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'宠物','http://op4axvn75.bkt.clouddn.com/%E5%AE%A0%E7%89%A9.png','chongwu');

#//第6类
#//美食：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'美食','http://op4axvn75.bkt.clouddn.com/%E7%BE%8E%E9%A3%9F.png','meishi');

#//第7类
#//运动：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'运动','http://op4axvn75.bkt.clouddn.com/%E8%BF%90%E5%8A%A8.png','ziren');

#//第8类
#//主题：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'主题','http://otooldst4.bkt.clouddn.com/%E4%B8%BB%E9%A2%98.png','专题 zhuanti zhuti');

#//第9类
#//节日：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'节日','http://otooldst4.bkt.clouddn.com/%E8%8A%82%E6%97%A5.png','假日 jiari');

#//第10类
#//商务：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'商务','http://op4axvn75.bkt.clouddn.com/LOGO.png','商业 shangye');

#//第11类
#//生活：
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,600,'生活','http://op4axvn75.bkt.clouddn.com/LOGO.png','shenghuo');

#//第12类
#//其他：
#由用户新增

#//自然/海滩/树林/瀑布/荒野/公路/落叶/小溪/大海/光/阳光/旅行
insert into t_com_tag(fid,source,levels,types,title,byname) values (1,1,2,600,'自然','ziren'),
(1,1,2,600,'海滩',''),
(1,1,2,600,'树林',''),
(1,1,2,600,'瀑布',''),
(1,1,2,600,'公路',''),
(1,1,2,600,'落叶',''),
(1,1,2,600,'小溪',''),
(1,1,2,600,'大海',''),
(1,1,2,600,'光',''),
(1,1,2,600,'阳光',''),
(1,1,2,600,'旅行','');

#//自然/花/春/花丛/树木/落叶/植物/园景/
insert into t_com_tag(fid, source,levels,types,title,byname) values (2,1,2,600,'花','hua'),
(2,1,2,600,'春',''),
(2,1,2,600,'花丛',''),
(2,1,2,600,'树木',''),
(2,1,2,600,'植物',''),
(2,1,2,600,'园景',''),
(2,1,2,600,'景点','');

#//女孩/美女/家人/男人/闺蜜/朋友/宝宝/美人/学生/熊孩子/孩童/青春
insert into t_com_tag( fid,source,levels,types,title,byname) values (3,1,2,600,'女孩','女儿 nvhai nvren nver'),
(3,1,2,600,'美女','美人'),
(3,1,2,600,'男人',''),
(3,1,2,600,'朋友',''),
(3,1,2,600,'宝宝',''),
(3,1,2,600,'学生',''),
(3,1,2,600,'熊孩子',''),
(3,1,2,600,'孩童',''),
(3,1,2,600,'女儿',''),
(3,1,2,600,'青春','');

#//开心/嗨/笑容/家人/爱/派对/社交/好吃
insert into t_com_tag(fid, source,levels,types,title,byname) values (4,1,2,600,'开心','kaixin happy'),
(4,1,2,600,'嗨',''),
(4,1,2,600,'笑容',''),
(4,1,2,600,'家人',''),
(4,1,2,600,'爱',''),
(4,1,2,600,'peace&love',''),
(4,1,2,600,'派对',''),
(4,1,2,600,'社交',''),
(4,1,2,600,'好吃','');

#//萌宠/猫/狗/动物
insert into t_com_tag( fid,source,levels,types,title,byname) values (5,1,2,600,'萌宠','宠物 chongmeng'),
(5,1,2,600,'猫','mao cat 英短'),
(5,1,2,600,'狗','gou dog 中华田园犬 哈士奇 柯基 二哈'),
(5,1,2,600,'动物','蜥蜴');

#//聚餐/宴会/喜酒/好吃/糕点/美酒
insert into t_com_tag( fid,source,levels,types,title,byname) values (6,1,2,600,'聚餐','大餐 聚会 饭 fan jucan juhui dacan'),
(6,1,2,600,'宴会','晚宴 wanhui wanyan'),
(6,1,2,600,'喜酒','xihuan 婚礼 婚庆 hunli'),
(6,1,2,600,'好吃','haochi 美食'),
(6,1,2,600,'糕点','gaoding'),
(6,1,2,600,'美酒','红酒 好酒 meijiu hongjiu'),
(6,1,2,600,'酒吧','jiuba');

#///健美/健身/跑步/瑜伽/体育/街舞/广场舞/舞蹈/游戏
insert into t_com_tag( fid,source,levels,types,title,byname) values (7,1,2,600,'健身','健美 jianshen jianmei'),
(7,1,2,600,'跑步','paobu'),
(7,1,2,600,'娱乐','yule KTV'),
(7,1,2,600,'游戏','youxi'),
(7,1,2,600,'瑜伽','yujia'),
(7,1,2,600,'体育','tiyu'),
(7,1,2,600,'街舞','jiewu'),
(7,1,2,600,'广场舞','guangchangwu'),
(7,1,2,600,'舞蹈','wudao');

#//艺术/背景/桌面/壁纸/创意/节日
insert into t_com_tag( fid,source,levels,types,title,byname) values (8,1,2,600,'艺术','yishu'),
(8,1,2,600,'背景','beijing'),
(8,1,2,600,'桌面','zhuomian'),
(8,1,2,600,'壁纸','bizhi'),
(8,1,2,600,'创意','chuangyi');

#//假日/假期/元旦/春节/元宵节/清明/五一/五四/青年节/端午节/中秋节/国庆节/妇女节/儿童节/双十一
insert into t_com_tag( fid,source,levels,types,title,byname) values (9,1,2,600,'假期',''),
(9,1,2,600,'元旦','新年 元月一号 元旦节yuandan '),
(9,1,2,600,'春节','新年 过年 chunjie guonian'),
(9,1,2,600,'元宵节','yuanxiaojie 汤圆 元宵'),
(9,1,2,600,'清明','清明节 踏青 郊游 qingming taqing jiaoyou'),
(9,1,2,600,'五一','放假 劳动节 wuyi laodongjie'),
(9,1,2,600,'五四','青年节 qingnian'),
(9,1,2,600,'妇女节','三八 38 女生节 sanba funv'),
(9,1,2,600,'端午节','粽子 屈原 duanwu zongzi quyuan'),
(9,1,2,600,'中秋节','月饼 月亮 赏月 yuebing moon shangyue yueliang'),
(9,1,2,600,'儿童节','孩子 宝宝 熊孩子 haizi xionghaizi'),
(9,1,2,600,'儿双十一','光棍 买买买 shuangshiyi guanggunjie');

#//会议/电脑/办公/财富/笔记本
insert into t_com_tag( fid,source,levels,types,title,byname) values (10,1,2,600,'会议','开会 峰会 kaihui huiyi'),
(10,1,2,600,'电脑','电脑桌 笔记本 diannao'),
(10,1,2,600,'办公','bangong'),
(10,1,2,600,'财富','钱 股票 发财 caifu qian'),
(10,1,2,600,'笔记本','bijiben 电子设备'),
(10,1,2,600,'手机','苹果 bijiben iphone');

#//生活
insert into t_com_tag( fid,source,levels,types,title,byname) values (11,1,2,600,'生活','shenghuo'),
(11,1,2,600,'娱乐','yule'),
(11,1,2,600,'书','shu'),
(11,1,2,600,'学习','xuexi'),
(11,1,2,600,'名胜古迹','景点 jingdian'),
(11,1,2,600,'学校','名校 武大 清华 北大 xuexiao'), 
(11,1,2,600,'旅游','周末 一日游 lvyou');  



#########################问卷分类标签#########################
insert into t_com_tag( source,levels,types,title,icon,byname) values (1,1,900,'问卷调查','http://op4axvn75.bkt.clouddn.com/LOGO.png','shenghuo');

#只有二级分类标签
insert into t_com_tag(source,types,title,byname) values (1,900,'市场调查','市场'),
(1,900,'大学生消费情况调查','大学生 消费'),
(1,900,'网购消费者行为调查','网购 消费'),
(1,900,'网购用户满意度调查','满意度'), 
(1,900,'新产品上市前调研','新品 产品'),
(1,900,'App使用习惯调查','App 使用'),
(1,900,'培训满意度调查','满意度'), 
(1,900,'旅游市场调查','旅游'), 
(1,900,'网站性能体验调研','网站'), 
(1,900,'手游用户偏好调研','手游'), 
(1,900,'趣味问答','趣味'); 
#set SQL_SAFE_UPDATES =0;
#update t_com_tag set fid = 698 where types = 900 and levels = 2

#常规banner（图片不带themeid = -1）是否显示
insert into t_com_ad(app,types,title,stime,etime) values
	 (11,2,"分类","2018-04-28 00:00:00","2028-05-10 00:00:00"),
	 (12,2,"分类","2018-04-28 00:00:00","2028-05-10 00:00:00");

#每天一个pop （不指定url不指定tags跳转整个themeid）
insert into t_com_adinfo(adid,types,title,stime,etime,img) values
	 (3,1,"看图旅行","2018-04-28 00:00:00","2028-04-28 23:59:59",'https://ccian.com/%E5%BC%B9%E7%AA%97%204.28.png');
	 
#banner（指定url）
insert into t_com_adinfo(adid,types,title,stime,etime,url,img) values
	 (3,2,"图买卖是什么","2018-04-28 00:00:00","2028-05-10 00:00:00","https://www.ccian.com/t/ad1/banner1.html",'https://ccian.com/tmm.png'),
	 (3,2,"韩妆礼品","2018-04-28 00:00:00","2028-05-10 00:00:00","https://www.ccian.com/t/ad1/banner3.html",'https://ccian.com/30%E5%A5%97%E9%9F%A9%E5%A6%86%E7%A4%BC%E5%93%81.png'),
	 (3,2,"福利路线","2018-04-28 00:00:00","2028-05-10 00:00:00","https://www.ccian.com/t/ad1/banner4.html",'https://ccian.com/%E5%9B%BE%E4%B9%B0%E5%8D%96%E7%A6%8F%E5%88%A9%E8%B7%AF%E7%BA%BF%E5%9B%BE.png'),
	 (3,2,"TOP100拍图王","2018-04-28 00:00:00","2028-05-10 00:00:00","https://www.ccian.com/t/ad1/banner2.html",'https://ccian.com/TOP100%E6%8B%8D%E5%9B%BE%E7%8E%8B%E6%94%AF%E6%8C%81.png');
	
#categories（指定tags）
insert into t_com_adinfo(adid,types,title,stime,etime,tags,img) values
	 (3,3,"城市","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"城市\",\"建筑\"]','https://ccian.com/city.png'),
	 (3,3,"旅游","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"旅行\",\"旅行\"]','https://ccian.com/travel.png'),
	 (3,3,"美女","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"美女\",\"女孩\"]','https://ccian.com/girls.png'),
	 (3,3,"美食","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"美食\",\"好吃\"]','https://ccian.com/foods.png'),
	 (3,3,"经济","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"经济\",\"商业\"]','https://ccian.com/economy.png'),
	 (3,3,"娱乐","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"娱乐\",\"开心\"]','https://ccian.com/amusement.png'),
	 (3,3,"萌宠","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"狗\",\"猫\"]','https://ccian.com/pets.png'),
	 (3,3,"生活","2018-04-28 00:00:00","2028-05-10 00:00:00",'[\"书\",\"花草\"]','https://ccian.com/life.png');

#相册活动
insert into t_com_adact(title,app,types,themeid,stime,etime) values
	 ('相册',11,100,-1,"2018-04-28 00:00:00","2018-05-10 00:00:00"),
	 ('主题',11,200,-1,"2018-04-28 00:00:00","2018-05-10 00:00:00"),
	 ('城市',11,300,2,"2018-04-28 00:00:00","2028-05-10 00:00:00");
	 
#categories（指定tags）
insert into t_map_album(title,source,types,themeid,logo) values
	 ('北京',1,300,2,'https://ccian.com/choosecity_beijing_img.png'),
	 ('上海',1,300,2,'https://ccian.com/choosecity_shanghai_img.png'),
	 ('广州',1,300,2,'https://ccian.com/choosecity_guangzhou_img.png'),
	 ('深圳',1,300,2,'https://ccian.com/choosecity_shenzhen_img.png'),
	 ('武汉',1,300,2,'https://ccian.com/choosecity_wuhan_img.png'),
	 ('杭州',1,300,2,'https://ccian.com/choosecity_hangzhou_img.png'),
	 ('成都',1,300,2,'https://ccian.com/choosecity_chegndu_img.png'),
	 ('重庆',1,300,2,'https://ccian.com/choosecity_chongqing_img.png'),
	 ('香港',1,300,2,'https://ccian.com/choosecity_xianggang_img.png'),
	 ('台北',1,300,2,'https://ccian.com/choosecity_taibei_img.png'),
	 ('东莞',1,300,2,'https://ccian.com/choosecity_dongguan_img.png'),
	 ('佛山',1,300,2,'https://ccian.com/choosecity_foushan_img.png'),
	 ('天津',1,300,2,'https://ccian.com/choosecity_tianjin_img.png'),
	 ('贵阳',1,300,2,'https://ccian.com/choosecity_guiyang_img.png'),
	 ('青岛',1,300,2,'https://ccian.com/choosecity_qingdao_img.png'),
	 ('郑州',1,300,2,'https://ccian.com/choosecity_zhengzhou_img.png');

